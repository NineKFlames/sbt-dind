# Sbt Dind

This repository contains a Dockerfile for a docker-in-docker image with SBT installed to enable Testcontainers use during tests.

CI is also in place for publishing the images to the project-local registry.

Image tag: `registry.gitlab.com/ninekflames/sbt-dind:latest`

Example usage: https://gitlab.com/NineKFlames/zio-cucumber/-/blob/main/.gitlab-ci.yml

### Credits:

* https://github.com/tloist/docker-alpine-sbt
* https://github.com/adoptium/containers
