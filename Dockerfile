FROM registry.gitlab.com/ninekflames/java-18-temurin-dind

ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

ARG SBT_VERSION="1.10.7"

RUN apk update &&\
    apk upgrade &&\
    apk add --no-cache bash tar

RUN apk add --no-cache --virtual=build-dependencies curl && \
    curl -sL "https://github.com/sbt/sbt/releases/download/v$SBT_VERSION/sbt-$SBT_VERSION.tgz" | gunzip | tar xvf - -C /usr/local && \
    ln -s /usr/local/sbt/bin/sbt /usr/local/bin/sbt && \
    chmod 0755 /usr/local/bin/sbt && \
    apk del build-dependencies \
